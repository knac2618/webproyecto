
const formulario  = document.getElementById("formulario");
const codigoVehiculo = document.getElementById("codigo");
const marca = document.getElementById("marca");
const modelo = document.getElementById("modelo");
const año = document.getElementById("año");
const fechainicial = document.getElementById("fechainicial");
const fechafinal = document.getElementById("fechafinal");

/*EXPRESIONES REGULARES*/
const validarcodigoVehiculo= new RegExp("[A-Za-z0-9]{5}+");
const validarMarca= new RegExp("[A-Za-z0-9]{5}+");
const validarModelo= new RegExp("[A-Za-z0-9]{5}+");
/*Solo número*/
const validaraño = /^\d+$/gi;
const validarfechafinal =new RegExp("/^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/");
const validarfechainicio = new RegExp("/^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/");

/*FUNCION VALIDAR FORMULARIO*/
formulario.addEventListener("submit", (e)=>{
e.preventDefault();

if(!validarcodigoVehiculo.test(codigoVehiculo) || !validaraño.test(año) || !validarMarca.test(marca) || !validarModelo.test(modelo) || !validarfechafinal.text(fechafinal) || !validarfechainicio.test(fechainicial)) {
    console.log("Dato no válido")
    return;

} })
